from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory

macros = widget.getEffectiveMacros()
macros.expandValues(macros)

mks = macros.getValue("FACEPLATE_MKS")
tpg = macros.getValue("FACEPLATE_TPG")

def bob_path(typ):
    if "tpg" in typ.lower():
        path = tpg
    elif "mks" in typ.lower():
        path = mks
    else:
        path = mks
        ScriptUtil.getLogger().severe("Cannot determine controller type: '{}', Falling back to MKS faceplate".format(typ))

    return path


controller      = macros.getValue("CONTROLLER")
controller_type = macros.getValue("CONTROLLER_TYPE")
devicename      = macros.getValue("vacPREFIX")
try:
    pumpPV = PVFactory.getPV("{}:iUITypeR".format(controller))
    pump = pumpPV.read()
    PVFactory.releasePV(pumpPV)
    pump = pump.value
    path = bob_path(pump)
except Exception as e:
    ScriptUtil.getLogger().severe(str(e))
    if controller_type:
        path = bob_path(controller_type)
    else:
        path = mks
        ScriptUtil.getLogger().severe("Falling back to mks faceplate")


macros.add("DEVICENAME", devicename)
dmacros = dict()
for k in macros.getNames():
    dmacros[k] = macros.getValue(k)

ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
