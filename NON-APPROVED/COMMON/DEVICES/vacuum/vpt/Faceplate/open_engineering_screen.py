from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory
from time import sleep

leybold  = "../../vept/Faceplate/vac_ctrl_leyboldtd20.bob"
pfeiffer = "../../vept/Faceplate/vac_ctrl_pfeiffertcp350.bob"

macros = widget.getEffectiveMacros()
macros.expandValues(macros)
controller = macros.getValue("CONTROLLER")
try:
    pumpPV = PVFactory.getPV("{}:iUITypeR".format(controller))
    pump = pumpPV.read()
    PVFactory.releasePV(pumpPV)
    pump = pump.value
    if "tcp350" in pump.lower():
        path = pfeiffer
    elif "td20" in pump.lower():
        path = leybold
    else:
        path = leybold
        ScriptUtil.getLogger().severe("Cannot determine controller type: '{}', Falling back to Leybold TD20 faceplate".format(pump))
except Exception as e:
    path = leybold
    ScriptUtil.getLogger().severe(str(e))
    ScriptUtil.getLogger().severe("Falling back to Leybold TD20 faceplate")


macros.add("DEVICENAME", controller)
dmacros = dict()
for k in macros.getNames():
    dmacros[k] = macros.getValue(k)

ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
