from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.properties import NamedWidgetColor
from org.csstudio.display.builder.model.persist import WidgetColorService

err_info = None

cond_no_error = 0
cond_error    = 1
cond_warning  = 2

try:
    error_code = PVUtil.getLong(pvs[1])

    if PVUtil.getLong(pvs[0]) != 2:
        code_table = dict({
                            0 :   0,
                            1 : 101,
                            3 : 103,
                            4 :   5,
                            6 : 106,
                            7 :   6,
                            8 :   8,
                           10 :   3,
                           16 : 116,
                           17 : 117,
                           19 :   2,
                           43 : 143,
                           60 :   4,
                           62 :  62
                          })
        try:
            error_code = code_table[error_code]
        except KeyError:
            if error_code > 100:
                error_code += 100

    msgs = dict({
                   0 : (cond_no_error, "No error"),

                 101 : (cond_error, "Overload warning\nP3 < P25*P24 (after normal operation has been attained) (not during generator operation!)."),

                 103 : (cond_error, "Power supply voltage error\nPower supply voltage failure during active pump operation."),

                   5 : (cond_error, "Converter temperature error\nP11 > limit threshold converter temperature."),

                 106 : (cond_error, "Overload error\nP3 < P20 after normal operation was attained."),

                   6 : (cond_error, "Run-up time error\nP3 < P24*P25 after P32 has elapsed with start signal being present."),

                   8 : (cond_error, "Pump error\nPump could not be identified or no pump has been connected."),

                   3 : (cond_error, "Pump temperature error\nP127 > P132 or temperature switch = ∞"),

                 116 : (cond_error, "Overload duration error\nP3 < P25*P24 longer of than P32."),

                 117 : (cond_error, "Motor current error\nNo motor current or motor current too low."),

                   2 : (cond_error, "Pass-through time error\n60 Hz < P3 < P20 after P183 has elapsed with the start signal being present"),

                 143 : (cond_error, "Internal error"),

                   4 : (cond_error, "Hardware monitoring\nShort-circuit within the motor or connecting cable (overcurrent, overvoltage, air cooler defective)"),

                  62 : (cond_error, "Pump temperature warning\nP127 > P128"),
                })

    try:
        err_info = msgs[error_code]
    except KeyError:
        if error_code > 200:
            err_info = (cond_error, "Internal error\nError within the converter or external voltage applied to the inputs: " + PVUtil.getString(pvs[1]))
        else:
            err_info = (cond_error, "Unknown error code: " + PVUtil.getString(pvs[1]))
            ScriptUtil.getLogger().severe("Unknown error code " + pvs[1] + " : " + PVUtil.getString(pvs[1]))
except Exception as err:
    ScriptUtil.getLogger().severe(str(err))
except:
    pass

tooltip = "$(pv_name)\n$(pv_value)"

if err_info is not None:
    msg = err_info[1]
    if isinstance(msg, list):
        tooltip = tooltip + "\n{code}: {msg}\nPossible causes:\n{causes}\nRemedy:\n{remedies}".format(code = code_str, msg = msg[0], causes = msg[1], remedies = msg[2])
    else:
        tooltip = tooltip + "\n{}".format(msg)

    cond = err_info[0]
    if cond == cond_error:
        color = WidgetColorService.getColor("RED-BACKGROUND")
    elif cond == cond_warning:
#        color = WidgetColorService.getColor("ORANGE")
        color = WidgetColorService.getColor("YELLOW-BACKGROUND")
    else:
        color = WidgetColorService.getColor("Read_Background")
else:
    color = WidgetColorService.getColor("Read_Background")

widget.setPropertyValue("tooltip", tooltip)
widget.setPropertyValue("background_color", color)
