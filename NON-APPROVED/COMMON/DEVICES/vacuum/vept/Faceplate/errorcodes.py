from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.properties import NamedWidgetColor
from org.csstudio.display.builder.model.persist import WidgetColorService

err_info = None

cond_no_error = 0
cond_error    = 1
cond_warning  = 2

try:
    code_str = PVUtil.getString(pvs[0])

    msgs = dict({
                "000000" : (cond_no_error, ""),
                # Errors
                "Err001" : (cond_error,
                            [ "Excess rotation speed",
                             "",
                             "+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                            ]),
                "Err002" : (cond_error,
                            [ "Overvoltage",
                             "- Incorrect mains input voltage",
                             "+ Check mains input voltage\n+ Only acknowledge for rotation speed f = 0\n+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err006" : (cond_error,
                            [ "Run-up time error",
                             "- Run-up time threshold set too low\n- Gas flow in vacuum chamber through leaks or open valves\n- Still below rotation speed switchpoint run-up time expires",
                             "+ Adjust run-up time to process conditions\n+ Check vacuum chamber for leaks and closed valves\n+ Check backup vacuum connection\n+ Adjust rotation speed switchpoint"
                            ]),
                "Err007" : (cond_error,
                            [ "Operating fluid low",
                             "- Operating fluid low",
                             "+ Check operating fluid\n+ Only acknowledge for rotation speed f = 0\n+ Can be acknowledged a max. of 5 times\n+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err015" : (cond_error,
                            [ "Group error message control unit",
                             "",
                             "+ Mains OFF/ON at rotation speed f = 0\n+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err021" : (cond_error,
                            [ "Electronic drive unit failed to identify pump",
                             "- Incorrect characteristic resistance\n- Pump not connected",
                             "+ Check connections\n+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                            ]),
                "Err037" : (cond_error,
                            [ "Motor final stage or control error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err040" : (cond_error,
                            [ "Memory expansion error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err043" : (cond_error,
                            [ "Internal configuration error",
                             "- Parameter values stored incorrectly",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err044" : (cond_error,
                            [ "Excess temperature electronics",
                             "- Insufficient cooling",
                             "+ Improve cooling\n+ Check deployment conditions"
                            ]),
                "Err045" : (cond_error,
                            [ "Motor temperature protection",
                             "- Motor overheated\n- Run-up time in lower speed range (up to 90 Hz) > 6 min",
                             "+ Improve cooling\n+ Check fore-vacuum connection\n   - Perform leak detection\n   - Reduce fore-vacuum pressure"
                            ]),
                "Err098" : (cond_error,
                            [ "Internal communication error",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err621" : (cond_error,
                            [ "Electronic drive unit failed to identify pump",
                             "- Incorrect characteristic resistance\n- Pump not connected",
                             "+ Check connections\n+ Contact Pfeiffer Vacuum Service\n+ Only acknowledge for rotation speed f = 0"
                            ]),
                "Err699" : (cond_error,
                            [ "Error in TCP drive",
                             "",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Err777" : (cond_error,
                            [ "Nominal speed not confirmed",
                             "- Nominal speed not confirmed after replacing electronic drive unit",
                             "+ Confirm nominal speed with [P:777]\n+ Only acknowledge for rotation speed f = 0"
                            ]),
                # Warnings
                "Wrn007" : (cond_warning,
                            [ "Undervoltage/mains failure",
                             "- Mains failure",
                             "+ Check power supply"
                            ]),
                "Wrn046" : (cond_warning,
                            [ "Data channel error",
                             "- Communication to parameter value memory faulty",
                             "+ Contact Pfeiffer Vacuum Service"
                            ]),
                "Wrn110" : (cond_warning,
                            [ "Gauge warning",
                             "- Gauge faulty\n- Supply cable worked loose during operation",
                             "+ Restart with gauge connected\n+ Replace gauge\n+ Install gauge correctly"
                            ]),
                })


    try:
        err_info = msgs[code_str]
    except KeyError:
        err_info = (cond_error, "\n{}: Unknown error code".format(code_str))
except Exception as err:
    ScriptUtil.getLogger().severe(str(err))
except:
    pass

tooltip = "$(pv_name)\n$(pv_value)"

if err_info is not None:
    msg = err_info[1]
    if isinstance(msg, list):
        tooltip = tooltip + "\n{code}: {msg}\nPossible causes:\n{causes}\nRemedy:\n{remedies}".format(code = code_str, msg = msg[0], causes = msg[1], remedies = msg[2])
    else:
        tooltip = tooltip + "\n{}".format(msg)

    cond = err_info[0]
    if cond == cond_error:
        color = WidgetColorService.getColor("RED-BACKGROUND")
    elif cond == cond_warning:
#        color = WidgetColorService.getColor("ORANGE")
        color = WidgetColorService.getColor("YELLOW-BACKGROUND")
    else:
        color = WidgetColorService.getColor("Read_Background")
else:
    color = WidgetColorService.getColor("Read_Background")

widget.setPropertyValue("tooltip", tooltip)
widget.setPropertyValue("background_color", color)
