<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title>ACCP Gas Management Panel</title>
    <save_changes>true</save_changes>
    <show_legend>true</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>true</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-1 hours 0.0 seconds</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </foreground>
    <background>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </background>
    <title_font>Cantarell|16|1</title_font>
    <label_font>Cantarell|11|1</label_font>
    <scale_font>Cantarell|10|0</scale_font>
    <legend_font>Cantarell|10|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>degreC</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>128</green>
                <blue>0</blue>
            </color>
            <min>-33.1619496855345</min>
            <max>1776.8223270440253</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>bar</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>0.5613207547169812</min>
            <max>22.02358490566038</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>g/s</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>24.056603773584907</min>
            <max>943.8679245283018</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>mbar</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>55</red>
                <green>188</green>
                <blue>233</blue>
            </color>
            <min>-73.47525653757032</min>
            <max>471.14986759351206</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>%</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>2.6729559748427674</min>
            <max>104.87421383647798</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>rpm</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>98.8993710691824</min>
            <max>3880.3459119496856</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Bar Low</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>30</red>
                <green>144</green>
                <blue>255</blue>
            </color>
            <min>-0.7347525653757042</min>
            <max>4.71149867593512</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Digital</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>-0.44085153922542136</min>
            <max>2.826899205561073</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Power</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>-235.12082092022504</min>
            <max>1507.6795762992388</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>vpm</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>253</red>
                <green>194</green>
                <blue>16</blue>
            </color>
            <min>-5.0</min>
            <max>50.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 1</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>243</red>
                <green>249</green>
                <blue>167</blue>
            </color>
            <min>-1.4695051307514084</min>
            <max>9.42299735187024</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 2</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <min>-0.4465721408474023</min>
            <max>2.0815776853690826</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>HP Pressure</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-PT-21632:Val</name>
            <axis>1</axis>
            <color>
                <red>222</red>
                <green>36</green>
                <blue>210</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>LP PRESSURE</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-PT-23099:Val</name>
            <axis>6</axis>
            <color>
                <red>0</red>
                <green>138</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>SP PRESSURE</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-PT-24099:Val</name>
            <axis>6</axis>
            <color>
                <red>0</red>
                <green>194</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>MP PRESSURE</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-PT-22631:Val</name>
            <axis>6</axis>
            <color>
                <red>5</red>
                <green>241</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>LP SPEED</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-M-23000:Val</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>SP SPEED</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-M-24000:Val</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>108</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Unload Valve Buffer A</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-GT-21600A:Val</name>
            <axis>4</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Load Valve Buffer A</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-GT-23600A:Val</name>
            <axis>4</axis>
            <color>
                <red>127</red>
                <green>127</green>
                <blue>127</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Buffer Pressure</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-PT-22600:Val</name>
            <axis>1</axis>
            <color>
                <red>229</red>
                <green>229</green>
                <blue>229</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Massflow HP</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-FT-21630:Val</name>
            <axis>2</axis>
            <color>
                <red>165</red>
                <green>42</green>
                <blue>42</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Massflow HP Bypass</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-FT-21631:Val</name>
            <axis>2</axis>
            <color>
                <red>127</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Massflow LP</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-FT-22630:Val</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>128</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-ACCP:CRYO-AI-219002:Val</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-AI-219002:Val</name>
            <axis>9</axis>
            <color>
                <red>255</red>
                <green>208</green>
                <blue>9</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Pwr-L1U2:CnPw-EA-ACM210:3Phase_ActiveP</display_name>
            <visible>true</visible>
            <name>Pwr-L1U2:CnPw-EA-ACM210:3Phase_ActiveP</name>
            <axis>0</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Oil conc.</display_name>
            <visible>true</visible>
            <name>CrS-ACCP:CRYO-AI-219004:Val</name>
            <axis>9</axis>
            <color>
                <red>105</red>
                <green>105</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>1.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>