<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title>Cold box</title>
    <save_changes>true</save_changes>
    <show_legend>true</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>true</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-2 months 0.0 seconds</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </foreground>
    <background>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </background>
    <title_font>Liberation Sans|20|1</title_font>
    <label_font>Liberation Sans|14|1</label_font>
    <scale_font>Liberation Sans|12|0</scale_font>
    <legend_font>Liberation Sans|14|0</legend_font>
    <axes>
        <axis>
            <visible>false</visible>
            <name>mbar</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>253.74812500000004</min>
            <max>291.03636217948724</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>K</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>44.246875</min>
            <max>302.32556089743593</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>g/s</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>1.046706875</min>
            <max>1.052033766025641</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>bar</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>1.3865946875000001</min>
            <max>1.4506092227564102</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>ppm</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>1.0120531249999998</min>
            <max>1.075925721153846</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>%</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>23.52531250000001</min>
            <max>41.8020592948718</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>Value 11</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>67.17348281250001</min>
            <max>67.23363994391026</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>GN2 outlet</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-36319:Val</name>
            <axis>1</axis>
            <color>
                <red>26</red>
                <green>228</green>
                <blue>236</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>ADS80K</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-31420:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>165</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TTT1 inlet</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-31450A:Val</name>
            <axis>1</axis>
            <color>
                <red>165</red>
                <green>42</green>
                <blue>42</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TTT1 outlet</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-31459A:Val</name>
            <axis>1</axis>
            <color>
                <red>30</red>
                <green>144</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TTT2 inlet</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-31450B:Val</name>
            <axis>1</axis>
            <color>
                <red>0</red>
                <green>128</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TTT2 outlet</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-31459B:Val</name>
            <axis>1</axis>
            <color>
                <red>144</red>
                <green>238</green>
                <blue>144</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TT Dewar</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-53606:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>47</green>
                <blue>150</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Heat exchanger</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-TE-31401A:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Dewar oulet to phase sep</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-33485:Out</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TT Phase sep 90L top</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-33470A:Val</name>
            <axis>1</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Dewar LP line</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-33470A:Out</name>
            <axis>5</axis>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>PT dewar</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-53612:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>JT valve dewar</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-31469A:Out</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>212</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Valve Bypass last exch</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-31465:Out</name>
            <axis>5</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>253</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-TE-31460A:Val</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-TE-31460A:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-TE-31460B:Val</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-TE-31460B:Val</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Turbines feed valve</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-31450:Out</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>207</green>
                <blue>146</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>P BP cold box</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-33359:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-C-100:SpeedFB</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-C-100:SpeedFB</name>
            <axis>6</axis>
            <color>
                <red>0</red>
                <green>127</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Level Phase sep</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PDT-33370:Val</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>253</green>
                <blue>239</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>JT  valve phase sep</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-CV-31469C:Out</name>
            <axis>5</axis>
            <color>
                <red>0</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>TT Phase sep 90L bottom</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-TE-33470B:Val</name>
            <axis>1</axis>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>127</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>Level Dewar</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PDT-53609:Val</name>
            <axis>5</axis>
            <color>
                <red>0</red>
                <green>127</green>
                <blue>63</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-LT-53607:Val</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-LT-53607:Val</name>
            <axis>2</axis>
            <color>
                <red>63</red>
                <green>0</green>
                <blue>127</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>