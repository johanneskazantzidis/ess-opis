<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title>ORS and compressors</title>
    <save_changes>true</save_changes>
    <show_legend>true</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>true</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-2 hours 0.0 seconds</start>
    <end>now</end>
    <archive_rescale>STAGGER</archive_rescale>
    <foreground>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </foreground>
    <background>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </background>
    <title_font>Liberation Sans|20|1</title_font>
    <label_font>Liberation Sans|14|1</label_font>
    <scale_font>Liberation Sans|12|0</scale_font>
    <legend_font>Liberation Sans|14|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>g/s</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>true</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>-0.0036100000000000004</min>
            <max>0.07581</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>BP stability</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>0.858</min>
            <max>1.1260000000000001</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>%</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>true</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>-346.0</min>
            <max>314.0</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>bar HP</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>1.143</min>
            <max>1.1674</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>false</visible>
            <name>bar</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>4.5</min>
            <max>31.1</max>
            <grid>false</grid>
            <autoscale>false</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 1</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <min>30.4163</min>
            <max>30.6077</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Value 2</name>
            <use_axis_name>false</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>8.385</min>
            <max>41.715</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>BP</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-23275:Val</name>
            <axis>1</axis>
            <color>
                <red>247</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>HP</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-21290:Val</name>
            <axis>4</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>bundle 3</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-39371:Val</name>
            <axis>4</axis>
            <color>
                <red>0</red>
                <green>46</green>
                <blue>255</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>buffer tank</display_name>
            <visible>false</visible>
            <name>CrS-PHS:Cryo-PI-1910:Pressure</name>
            <axis>4</axis>
            <color>
                <red>144</red>
                <green>238</green>
                <blue>144</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-FT-23920:Counter</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-FT-23920:Val</name>
            <axis>0</axis>
            <color>
                <red>248</red>
                <green>127</green>
                <blue>5</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-PT-33370:Val</display_name>
            <visible>false</visible>
            <name>CrS-TICP:Cryo-PT-33370:Val</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>165</green>
                <blue>0</blue>
            </color>
            <trace_type>SINGLE_LINE</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-CV-31469B:Out</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-CV-31469B:Out</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-LT-53607:Val</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-LT-53607:Val</name>
            <axis>5</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>CrS-TICP:Cryo-LT-75170:Val</display_name>
            <visible>true</visible>
            <name>CrS-TICP:Cryo-LT-75170:Val</name>
            <axis>6</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.ics.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>