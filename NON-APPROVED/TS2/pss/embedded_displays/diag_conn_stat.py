from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

visible = True
tooltip = """Unknown connection status
$(pv_name)
$(pv_value)"""

try:
    pvSev0 = PVUtil.getSeverity(pvs[0])
    pvInt0 = PVUtil.getLong(pvs[0])

    if pvInt0 == True and pvSev0 == 0:
        visible = False
    else:
        tooltip = """{}!
$(pv_name)
$(pv_value)""".format(PVUtil.getString(pvs[0]))
except:
    pass

widget.setPropertyValue('visible', visible)
widget.setPropertyValue('tooltip', tooltip)
